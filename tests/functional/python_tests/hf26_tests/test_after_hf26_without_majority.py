from .local_tools import prepare_wallets, legacy_operation_passed, hf26_operation_failed

# def test_after_hf26_without_majority(network_after_hf26_without_majority):
#     wallet_legacy, wallet_hf26 = prepare_wallets(network_after_hf26_without_majority)

#     legacy_operation_passed(wallet_legacy)
#     hf26_operation_failed(wallet_hf26)
